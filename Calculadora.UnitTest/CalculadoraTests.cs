using NUnit.Framework;
using Calculadora;

namespace Calculadora.UnitTest
{
    public class CalculadoraTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void ExecutarCalculo_QuandoAcaoForSomar_retornaSoma()
        {
            //arrange
            var calculadora = new calculo();

            calculadora.campo1 = 2;
            calculadora.campo2 = 1;
            calculadora.tipodecalculo = "+";

            //act
            var resultado = calculadora.Total();


            //assert
            Assert.That(resultado, Is.EqualTo(3));


            
        }

        public void ExecutarCalculo_QuandoAcaoForSubtrair_retornaSoma()
        {
            //arrange
            var calculadora = new calculo();

            calculadora.campo1 = 2;
            calculadora.campo2 = 1;
            calculadora.tipodecalculo = "-";

            //act
            var resultado = calculadora.Total();


            //assert
            Assert.That(resultado, Is.EqualTo(1));



        }

        public void ExecutarCalculo_QuandoAcaoForMultiplicar_retornaSoma()
        {
            //arrange
            var calculadora = new calculo();

            calculadora.campo1 = 2;
            calculadora.campo2 = 1;
            calculadora.tipodecalculo = "*";

            //act
            var resultado = calculadora.Total();


            //assert
            Assert.That(resultado, Is.EqualTo(2));



        }

        public void ExecutarCalculo_QuandoAcaoForDividir_retornaSoma()
        {
            //arrange
            var calculadora = new calculo();

            calculadora.campo1 = 2;
            calculadora.campo2 = 1;
            calculadora.tipodecalculo = "/";

            //act
            var resultado = calculadora.Total();


            //assert
            Assert.That(resultado, Is.EqualTo(2));



        }
        public void ExecutarCalculo_QuandoAcaoDivisaofor0_retornaSoma()
        {
            //arrange
            var calculadora = new calculo();

            calculadora.campo1 = 2;
            calculadora.campo2 = 0;
            calculadora.tipodecalculo = "/";

            //act
            var resultado = calculadora.Total();


            //assert
            Assert.That(resultado, Is.EqualTo(3));

        }
    }
}